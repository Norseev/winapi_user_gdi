﻿<topic template="Default" lasteditedby="Sergey" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">IsCharAlphaNumeric</title>
  <keywords>
    <keyword translate="true">IsCharAlphaNumeric</keyword>
    <keyword translate="true">user32</keyword>
    <keyword translate="true">winuser</keyword>
  </keywords>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">IsCharAlphaNumeric</text></para>
    </header>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Определяет является ли заданный символ алфавитно-цифровым. Данная проверка основывается на языковых настройках, указанных пользователем во время установки операционной системы или настроенных им в Панели Управления.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para style="text-align:justify;"><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Прототип</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">BOOL WINAPI IsCharAlphaNumeric(</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160;_In_ TCHAR ch</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">);</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para style="text-align:justify;"><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Параметры</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" style="font-style:italic;" translate="true">ch [in]</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Проверяемый символ.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Возвращаемое значение</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Если символ является алфавитно-цифровым, то функция возвращает ненулевое значение.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Если &#160;символ не является алфавитно-цифровым, то функция возвращает значение нуль.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Возвращаемое значение в случае ошибки</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Единственный способ определить факт ошибки - использовать функцию </text><link displaytype="text" defaultstyle="true" type="topiclink" href="GetLastError" styleclass="Normal" translate="true">GetLastError</link><text styleclass="Normal" translate="true">.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Пример использования</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Ниже приводится пример использования данной функции.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#008000;" translate="true">#include &lt;windows.h&gt;</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">int</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> WINAPI WinMain (HINSTANCE hThisInstance,</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;HINSTANCE hPrevInstance,</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;LPSTR lpszArgument,</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">int</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> nFunsterStil)</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">{ &#160; </text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text><text styleclass="Code Example" style="font-weight:normal; font-style:italic; color:#000080;" translate="true">//Тестируемый символ</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">char</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> ch = </text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">&apos;Щ&apos;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">;</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text><text styleclass="Code Example" style="font-weight:normal; font-style:italic; color:#000080;" translate="true">//Тестируем символ</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">if</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">(IsCharAlphaNumeric(ch))</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; &#160; &#160;MessageBox(NULL, </text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">&quot;Алфавитно-цифровой&quot;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">, </text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">&quot;&quot;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">, MB_OK);</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">else</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; &#160; &#160;MessageBox(NULL, </text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">&quot;Не алфавитно-цифровой&quot;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">, </text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">&quot;&quot;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">, MB_OK); &#160; </text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; &#160; </text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160; </text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">return</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">&#32;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">0</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">;</text></para>
    <para styleclass="Code Example" style="text-indent:48px; margin-right:0px; margin-left:0px;"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">}</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">В результате работы этой программы на экран выводится сообщение &quot;Алфавитно-цифровой&quot;.</text></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"></para>
    <para style="text-align:justify;"><table styleclass="Default" rowcount="3" colcount="3">
      <tr style="vertical-align:top">
        <td style="width:204px;">
          <para><text style="font-family:Arial; font-size:10pt; font-weight:bold; color:#000000;" translate="true">Заголовочный файл</text><text style="font-family:Arial; font-size:10pt; color:#000000;" translate="true"> &#160; &#160;</text></para>
        </td>
        <td style="width:15px;">
        </td>
        <td style="width:210px;">
          <para styleclass="Normal"><text style="font-family:Arial; font-size:10pt; color:#000000;" translate="true">winuser.h (подключен в windows.h)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:204px;">
          <para><text style="font-family:Arial; font-size:10pt; font-weight:bold; color:#000000;" translate="true">Экспортирующая библиотека</text></para>
        </td>
        <td style="width:15px;">
        </td>
        <td style="width:210px;">
          <para styleclass="Normal"><text style="font-family:Arial; font-size:10pt; color:#000000;" translate="true">user32.dll</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:204px;">
          <para><text style="font-family:Arial; font-size:10pt; font-weight:bold; color:#000000;" translate="true">ANSI и Unicode наименования</text></para>
        </td>
        <td style="width:15px;">
        </td>
        <td style="width:210px;">
          <para styleclass="Normal"><text style="font-family:Arial; font-size:10pt; color:#000000;" translate="true">IsCharAlphaNumericA и IsCharAlphaNumericW</text></para>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
