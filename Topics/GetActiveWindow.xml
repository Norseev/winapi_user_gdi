﻿<topic template="Default" lasteditedby="Sergey" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">GetActiveWindow</title>
  <keywords>
    <keyword translate="true">GetActiveWindow</keyword>
    <keyword translate="true">user32</keyword>
    <keyword translate="true">winuser</keyword>
  </keywords>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">GetActiveWindow</text></para>
    </header>
    <para styleclass="Normal" style="text-indent:51px; margin-right:0px; margin-left:0px;"></para>
    <para styleclass="Normal" style="text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Возвращает описатель активного окна, связанного с очередью сообщений текущего потока.</text></para>
    <para styleclass="Normal"></para>
    <para><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Прототип</text></para>
    <para styleclass="Normal"></para>
    <para styleclass="Normal"><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">HWND WINAPI GetActiveWindow(</text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">void</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">);</text></para>
    <para styleclass="Normal"></para>
    <para><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Возвращаемое значение</text></para>
    <para styleclass="Normal"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Возвращает описатель активного окна, связанного с очередью сообщений текущего потока. Если с текущим (вызывающим функцию) потоком не связано ни одного окна, функция возвращает значение NULL.</text></para>
    <para styleclass="Normal" style="text-align:justify;"></para>
    <para><text style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:bold; color:#000000;" translate="true">Пример использования</text></para>
    <para styleclass="Normal" style="text-align:justify;"><text styleclass="Normal" translate="true"> &#160;</text></para>
    <para styleclass="Code Example"><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#008000;" translate="true">#include &lt;stdio.h&gt;</text></para>
    <para styleclass="Code Example"><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#008000;" translate="true">#include &lt;stdlib.h&gt;</text></para>
    <para styleclass="Code Example"><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#008000;" translate="true">#include &lt;windows.h&gt;</text></para>
    <para styleclass="Code Example"></para>
    <para styleclass="Code Example"><tab /><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">int</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> main(</text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">int</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> argc, </text><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">char</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> *argv[])</text></para>
    <para styleclass="Code Example"><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">{</text></para>
    <para styleclass="Code Example"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160;</text><tab /><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">HWND hwnd;</text></para>
    <para styleclass="Code Example"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160;</text><tab /><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">hwnd = GetActiveWindow();</text></para>
    <para styleclass="Code Example"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160;</text><tab /><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">printf(</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">&quot;%x\n&quot;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">,hwnd); &#160;</text></para>
    <para styleclass="Code Example"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160;</text><tab /><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">getch(); &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text></para>
    <para styleclass="Code Example"><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true"> &#160;</text><tab /><tab /><text styleclass="Code Example" style="font-weight:bold; font-style:normal; color:#000000;" translate="true">return</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">&#32;</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000080;" translate="true">0</text><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">;</text></para>
    <para styleclass="Code Example"><tab /><text styleclass="Code Example" style="font-weight:normal; font-style:normal; color:#000000;" translate="true">}</text></para>
    <para styleclass="Normal" style="text-align:justify;"></para>
    <para styleclass="Normal" style="text-align:justify; text-indent:49px; margin-right:0px; margin-left:0px;"><text styleclass="Normal" translate="true">Так как в данном примере с основным потоком приложения не связано ни одного окна, то функция GetActiveWindow возвращает значение NULL.</text></para>
    <para styleclass="Normal" style="text-align:justify;"></para>
    <para styleclass="Normal" style="text-align:justify;"></para>
    <para styleclass="Normal" style="text-align:justify;"></para>
    <para styleclass="Normal"><table styleclass="Default" rowcount="2" colcount="3">
      <tr style="vertical-align:top">
        <td style="width:197px;">
          <para styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Заголовочный файл</text></para>
        </td>
        <td style="width:31px;">
        </td>
        <td style="width:203px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">winuser.h (включен в windows.h)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:197px;">
          <para styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Экспортирующая библиотека</text></para>
        </td>
        <td style="width:31px;">
        </td>
        <td style="width:203px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">user32.dll</text></para>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
